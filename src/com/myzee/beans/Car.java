package com.myzee.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class Car {
	private String carName;
	
	//@Qualifier(value="e1")
	@Autowired
	private Engine engine;
	
	public void setCarName(String carName) {
		this.carName = carName;
	}
	
	public void printData() {
		System.out.println(this.carName + ":" + this.engine.getModelYear());
	}
	
}
